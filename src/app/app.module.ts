import { BrowserModule, Title } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {
  NgModule,
  ApplicationRef
} from '@angular/core';
import {
  removeNgStyles,
  createNewHosts,
  createInputTransfer
} from '@angularclass/hmr';
import {
  RouterModule,
  PreloadAllModules
} from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';

/*
 * Platform and Environment providers/directives/pipes
 */
import { ENV_PROVIDERS } from './environment';
import { ROUTES } from './app.routes';
// App is our top level component
import { AppComponent } from './app.component';
import { APP_RESOLVER_PROVIDERS } from './app.resolver';
import { AppState, InternalStateType } from './app.service';
import { HomeComponent } from './home';
import { AboutComponent } from './about';
import { NoContentComponent } from './no-content';
import { XLargeDirective } from './home/x-large';

import '../styles/styles.scss';
import '../styles/headings.css';

/**
 * mQuBits
 */
import { OAuthService } from './mQuBits/services/oauth/o-auth.service';
import { Handler } from './mQuBits/services/handler';
import { HeaderComponent } from './mQuBits/components/layout/header/header.component';
import { HowItWorksComponent } from './mQuBits/components/layout/howitworks/howitworks.component';
import { WhyUsComponent } from './mQuBits/components/layout/whyus/whyus.component';
import { BecomePremiumComponent } from './mQuBits/components/layout/becomepremium/becomepremium.component';
import { FooterComponent } from './mQuBits/components/layout/footer/footer.component';
import { FrontpageComponent } from './mQuBits/components/layout/frontpage/frontpage.component';
import { CatalogComponent } from './mQuBits/components/products/catalog/catalog.component';
import { ScrapComponent } from './mQuBits/components/products/scrap/scrap.component';
import { LoginComponent } from './mQuBits/components/users/login/login.component';
import { RegisterComponent } from './mQuBits/components/users/register/register.component';
import { SubscribePopupComponent } from './mQuBits/components/layout/subscribepopup/subscribepopup.component';
import { ProfileComponent } from './mQuBits/components/users/profile/profile.component';
import { SearchPageComponent } from './mQuBits/components/products/searchpage/searchpage.component';
import { MQRestifyPipe } from './mQuBits/pipes/m-q-restify.pipe';
import { SearchComponent } from './mQuBits/components/layout/search/search.component';
import { CouponComponent } from './mQuBits/components/products/coupon/coupon.component';
import { CheckoutComponent } from './mQuBits/components/ecommerce/checkout/checkout.component';
import { SubscribeComponent } from './mQuBits/components/layout/Subscribe/subscribe.component';
import { CartComponent } from './mQuBits/components/ecommerce/cart/cart.component';
import { ItemComponent } from './mQuBits/components/products/item/item.component';
import { BrandsComponent } from './mQuBits/components/brands/brands/brands.component';
import { BrandComponent } from './mQuBits/components/brands/brand/brand.component';
import { SearchFormComponent } from './mQuBits/components/layout/search-form/search-form.component';
import { StickybarComponent } from './mQuBits/components/layout/stickybar/stickybar.component';
import { ListingProductsByCatalogComponent } from './mQuBits/components/products/listing-products-by-catalog/listing-products-by-catalog.component';
import { IterateObject } from './mQuBits/pipes/iterate.object.pipe';
import { CategoryComponent } from './mQuBits/components/products/category/category.component';
import { SizechartComponent } from './mQuBits/components/sizechart/sizechart.component';
import { LoaderService } from './mQuBits/services/loader/loader.service';
import { CategorylandingpageComponent } from './mQuBits/components/category-landing-page/category-landing-page.component';
import { BannersComponent } from './mQuBits/components/banners/banners.component';
import { TopBrandsComponent } from './mQuBits/components/brands/topbrands/topbrands.component';
import { CattreeComponent } from './mQuBits/components/cat-tree/cat-tree.component';
import { CommingSoonComponent } from './mQuBits/components/layout/commingsoon/comming-soon.component';
import { AuthComponent } from './mQuBits/components/users/auth/auth.component';
import { ContactUsComponent } from './mQuBits/components/contact-us/contact-us.component';
import { CouponsHistoryComponent } from './mQuBits/components/layout/CouponsHistory/coupons-history.component';
import { ScrollComponent } from './mQuBits/components/layout/scroll/scroll.component';
import { TrackComponent } from './mQuBits/components/products/trackorder/trackorder.component';
import { LiveCouponComponent } from './mQuBits/components/products/live-coupon/live-coupon.component';
import { PromotionsComponent } from './mQuBits/components/promotions/promotions.component';
/**
 * CommingSoonComponent
 * 3rd Party
 */
import { NguiDatetimePickerModule } from '@ngui/datetime-picker';
import { Ng2WizardModule } from 'ng2-wizard';
import { SwiperModule } from 'angular2-useful-swiper';
import {ImageZoomModule} from 'angular2-image-zoom';

// Application wide providers TrackorderComponent CartComponent
const APP_PROVIDERS = [
  ...APP_RESOLVER_PROVIDERS,
  AppState
];

type StoreType = {
  state: InternalStateType,
  restoreInputValues: () => void,
  disposeOldHosts: () => void
};

/**
 * `AppModule` is the main entry point into Angular2's bootstraping process
 */
@NgModule({
  bootstrap: [AppComponent],
  declarations: [
    AppComponent,
    AboutComponent,
    HomeComponent,
    CatalogComponent,
    NoContentComponent,
    XLargeDirective,
    HeaderComponent,
    HowItWorksComponent,
    WhyUsComponent,
    BecomePremiumComponent,
    FooterComponent,
    FrontpageComponent,
    ScrapComponent,
    LoginComponent,
    RegisterComponent,
    SubscribePopupComponent,
    ProfileComponent,
    SearchPageComponent,
    MQRestifyPipe,
    CheckoutComponent,
    SearchComponent,
    CouponComponent,
    SubscribeComponent,
    CartComponent,
    ItemComponent,
    BrandsComponent,
    BrandComponent,
    SearchFormComponent,
    StickybarComponent,
    ListingProductsByCatalogComponent,
    IterateObject,
    CategoryComponent,
    SizechartComponent,
    CategorylandingpageComponent,
    BannersComponent,
    TopBrandsComponent,
    CattreeComponent,
    CommingSoonComponent,
    AuthComponent,
    ContactUsComponent,
    ScrollComponent,
    TrackComponent,
    CouponsHistoryComponent,
    LiveCouponComponent,
    PromotionsComponent
  ],
  imports: [ // import Angular's modules
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(ROUTES, { useHash: true, preloadingStrategy: PreloadAllModules }),
    ReactiveFormsModule,
    NguiDatetimePickerModule,
    Ng2WizardModule,
    SwiperModule,
    ImageZoomModule
  ],
  providers: [ // expose our Services and Providers into Angular's dependency injection
    ENV_PROVIDERS,
    APP_PROVIDERS,
    OAuthService,
    Handler,
    Title,
    LoaderService
  ]
})
export class AppModule {

  constructor(
    public appRef: ApplicationRef,
    public appState: AppState
  ) { }

  public hmrOnInit(store: StoreType) {
    if (!store || !store.state) {
      return;
    }
    // set state
    this.appState._state = store.state;
    // set input values
    if ('restoreInputValues' in store) {
      let restoreInputValues = store.restoreInputValues;
      setTimeout(restoreInputValues);
    }

    this.appRef.tick();
    delete store.state;
    delete store.restoreInputValues;
  }

  public hmrOnDestroy(store: StoreType) {
    const cmpLocation = this.appRef.components.map((cmp) => cmp.location.nativeElement);
    // save state
    const state = this.appState._state;
    store.state = state;
    // recreate root elements
    store.disposeOldHosts = createNewHosts(cmpLocation);
    // save input values
    store.restoreInputValues = createInputTransfer();
    // remove styles
    removeNgStyles();
  }

  public hmrAfterDestroy(store: StoreType) {
    // display new elements
    store.disposeOldHosts();
    delete store.disposeOldHosts;
  }

}
