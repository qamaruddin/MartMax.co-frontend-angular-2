export const CATALOGS = [
    {
        keyword: 'Nike',
        category: 'Shoes',
        pagination: {
            number: 1,
            size: 10
        }
    }
    ,
    {
        keyword: 'shoes',
        category: 'All',
        pagination: {
            number: 1,
            size: 10
        }
    }
    ,
    {
        keyword: 'laptop',
        category: 'All',
        pagination: {
            number: 1,
            size: 10
        }
    }
];
