import { ActivatedRoute, Data } from '@angular/router';
import { Component } from '@angular/core';
import { inject, TestBed } from '@angular/core/testing';

// Load the implementations that should be tested
import { CheckoutComponent } from './checkout.component';

describe('Checkout', () => {
  // provide our implementations or mocks to the dependency injector
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      // provide a better mock
      {
        provide: ActivatedRoute,
        useValue: {
          data: {
            subscribe: (fn: (value: Data) => void) => fn({
              yourData: 'yolo'
            })
          }
        }
      },
      CheckoutComponent
    ]
  }));

  it('should log ngOnInit', inject([CheckoutComponent], (checkout: CheckoutComponent) => {
    spyOn(console, 'log');
    expect(console.log).not.toHaveBeenCalled();
    checkout.ngOnInit();
    expect(console.log).toHaveBeenCalled();
  }));

});
