/**
 * @author Mustafa Omran <m.omran@mQuBits.com>
 */

import {
  Component,
  OnInit
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Config } from './../../../environments/config';
import { CouponsService } from './../../../services/coupons/coupons.service';
import { Coupon } from './../../../models/coupon.model';

/**
 *  jQuery variables
 */
declare var jquery: any;
declare var $: any;

@Component({
  selector: 'martmax-coupon',
  styleUrls: ['./coupon.component.css'],
  templateUrl: './coupon.component.html',
  providers: [CouponsService],
})
export class CouponComponent implements OnInit {
  public errors: any;
  public coupon: Coupon;
  public countdown: any;

  constructor(
    public route: ActivatedRoute,
    public couponsService: CouponsService,
  ) {
  }

  public ngOnInit() {
    $(function () {
      $('.clickable').on('click', function () {
        var effect = $(this).data('effect');
        $(this).closest('.remove')[effect]();
      })
    })

    /* coupons service */
    this.couponsService.list()
      .subscribe(
      (data) => {
        if (data.length === 0) {
          return;
        }
        let myCoupon = new Coupon(data.id, data.code, data.type, data.value, data.redeem_datetime, data.deleted_at, data.created_at, data.updated_at);
        this.coupon = myCoupon;
        this.countdown = this.coupon.timer();
      },
      (errors) => {
        this.errors = errors;
      }
      );
  }


  /**
   * copy text to clipboard
   */
  public copyToClipboard() {
    let x = document.createElement('input');
    x.setAttribute('value', document.getElementById('cpy-value').innerHTML);
    document.body.appendChild(x);
    x.select();
    if (document.execCommand('copy')) {
      let y;
      y = document.getElementById('cpy-btn');
      y.innerHTML = 'Copied <i class="glyphicon glyphicon-ok"></i>';
      y.setAttribute('class', 'btn btn-success');
    }
  }
}


