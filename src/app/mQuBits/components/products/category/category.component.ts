/**
 * @author Mustafa Qamar-ud-Din <m.qamaruddin@mQuBits.com>
  * @author Abdelazem Eid <a.eid@mQuBits.com>
 */
import {
  Component,
  OnInit
} from '@angular/core';
import {
  Router,
  ActivatedRoute
} from '@angular/router';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Config } from './../../../environments/config';
import { CategoriesService } from './../../../services/products/categories.service';

/**
 *  jQuery variables
 */
declare var jquery: any;
declare var $: any;

@Component({
  selector: 'category',
  styleUrls: ['./category.component.css'],
  templateUrl: './category.component.html',
  providers: [CategoriesService],
})
export class CategoryComponent implements OnInit {

  config: Object = {
    pagination: '.swiper-pagination',
    paginationClickable: true,
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
    slidesPerView: 1,
    autoplay: 2500,
    loop: true,
  };

  public errors: any;
  public categories: any;
  public formErrors = {
    category: '',
    keyword: ''
  };

  /**
   * Form Controls
   */
  public searchForm: FormGroup;

  constructor(
    public route: ActivatedRoute,
    public categoriesService: CategoriesService,
    private fb: FormBuilder,
    private router: Router
  ) {

  }

  public ngOnInit() {
    $(".nav-tabs > li > a").mouseover(function () {
      $(this).tab('show');
    });
    
    this.createForm();
    this.categoriesService.list()
      .subscribe(
      (data) => {
        this.categories = data;
      },
      (errors) => {
        this.errors = errors;
      }
      );
  }

  public createForm() {
    this.searchForm = this.fb.group({
      category: ['', Validators.required],
      keyword: ['', Validators.required]
    });

    this.searchForm.valueChanges.subscribe((data) => (this.onValueChanged(data)));

    this.onValueChanged();
  }

  public onValueChanged(data?: any) {
    if (!this.searchForm) {
      return;
    }
    for (let key in this.formErrors) {
      if (!this.formErrors[key]) {
        continue;
      }
      this.formErrors[key] = '';
    }
  }

  public onSubmit() {
    this.router.navigate([
      'products/search',
      this.searchForm.value.category,
      this.searchForm.value.keyword
    ]);
  }

}