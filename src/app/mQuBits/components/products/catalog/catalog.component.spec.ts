import { ActivatedRoute, Data } from '@angular/router';
import { Component } from '@angular/core';
import { inject, TestBed } from '@angular/core/testing';

// Load the implementations that should be tested
import { CatalogComponent } from './catalog.component';

describe('Catalog', () => {
  // provide our implementations or mocks to the dependency injector
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      // provide a better mock
      {
        provide: ActivatedRoute,
        useValue: {
          data: {
            subscribe: (fn: (value: Data) => void) => fn({
              yourData: 'yolo'
            })
          }
        }
      },
      CatalogComponent
    ]
  }));

  it('should log ngOnInit', inject([CatalogComponent], (catalog: CatalogComponent) => {
    spyOn(console, 'log');
    expect(console.log).not.toHaveBeenCalled();

    catalog.ngOnInit();
    expect(console.log).toHaveBeenCalled();
  }));

});
