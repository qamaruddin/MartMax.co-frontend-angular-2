import { ActivatedRoute, Data } from '@angular/router';
import { Component } from '@angular/core';
import { inject, TestBed } from '@angular/core/testing';

// Load the implementations that should be tested
import { ListingProductsByCatalogComponent } from './listingproductsbycatalog.component';

describe('Listing', () => {
  // provide our implementations or mocks to the dependency injector
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      // provide a better mock
      {
        provide: ActivatedRoute,
        useValue: {
          data: {
            subscribe: (fn: (value: Data) => void) => fn({
              yourData: 'yolo'
            })
          }
        }
      },
      ListingProductsByCatalogComponent
    ]
  }));

  it('should log ngOnInit', inject([ListingProductsByCatalogComponent], (listingproducts: ListingProductsByCatalogComponent) => {
    spyOn(console, 'log');
    expect(console.log).not.toHaveBeenCalled();
    listingproducts.ngOnInit();
    expect(console.log).toHaveBeenCalled();
  }));

});
