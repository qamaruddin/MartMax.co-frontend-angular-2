import { ActivatedRoute, Data } from '@angular/router';
import { Component } from '@angular/core';
import { inject, TestBed } from '@angular/core/testing';

// Load the implementations that should be tested
import { SearchPageComponent } from './searchpage.component';

describe('SearchPage', () => {
  // provide our implementations or mocks to the dependency injector
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      // provide a better mock
      {
        provide: ActivatedRoute,
        useValue: {
          data: {
            subscribe: (fn: (value: Data) => void) => fn({
              yourData: 'yolo'
            })
          }
        }
      },
      SearchPageComponent
    ]
  }));

  it('should log ngOnInit', inject([SearchPageComponent], (searchpage: SearchPageComponent) => {
    spyOn(console, 'log');
    expect(console.log).not.toHaveBeenCalled();
    searchpage.ngOnInit();
    expect(console.log).toHaveBeenCalled();
  }));

});
