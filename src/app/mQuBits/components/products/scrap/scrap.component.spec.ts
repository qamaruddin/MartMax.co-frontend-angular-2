import { ActivatedRoute, Data } from '@angular/router';
import { Component } from '@angular/core';
import { inject, TestBed } from '@angular/core/testing';

// Load the implementations that should be tested
import { ScrapComponent } from './scrap.component';

describe('Scrap', () => {
  // provide our implementations or mocks to the dependency injector
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      // provide a better mock
      {
        provide: ActivatedRoute,
        useValue: {
          data: {
            subscribe: (fn: (value: Data) => void) => fn({
              yourData: 'yolo'
            })
          }
        }
      },
      ScrapComponent
    ]
  }));

  it('should log ngOnInit', inject([ScrapComponent], (scrap: ScrapComponent) => {
    spyOn(console, 'log');
    expect(console.log).not.toHaveBeenCalled();

    scrap.ngOnInit();
    expect(console.log).toHaveBeenCalled();
  }));

});
