/**
 * @author Mustafa Qamar-ud-Din <m.qamaruddin@mQuBits.com>
 */
import {
	Component,
	OnInit,
	Input,
	Attribute,
	ElementRef,
	EventEmitter,
	Output
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PromotionsService } from './../../services/products/promotions.service';
/**
 *  jQuery variables
 */
declare var jquery: any;
declare var $: any;

@Component({
	selector: 'promotions',
	styleUrls: ['./promotions.component.css'],
	templateUrl: './promotions.component.html',
	providers: [
		PromotionsService
	]
})

export class PromotionsComponent implements OnInit {

    public products: any;
    public errors: any;
    public config: any;
    public page: number;
    public label: string;

    constructor(
       public route: ActivatedRoute,
       public promotionsService: PromotionsService
    ) {
       this.page = 1;
    }

    public ngOnInit() {
      this.route.params.subscribe( (params) => {
        this.label = params['label'];
        this.promotionsService.search({
            label: params['label'],
            page: this.page
        }).subscribe(
                (data) => {
                   this.products = [];
                   Object.keys(data).map( (key) => {
                         this.products.push(data[key]);
                   });
                },
                (errors) => {
                   this.errors = errors;
                }
            );
      });
    }

    public onScroll(event) {
       this.page += 1;
       this.promotionsService.search({
            label: this.label,
            page: this.page
        }).subscribe(
                (data) => {
                   Object.keys(data).map( (key) => {
                         this.products.push(data[key]);
                   });
                },
                (errors) => {
                   this.errors = errors;
                }
        );
    }
}
