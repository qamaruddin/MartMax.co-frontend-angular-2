/** 
*@author Mustafa Omran <m.omran@mQuBits.com>
*/

import {
  Component,
  OnInit
} from '@angular/core';
import {
  Router,
  ActivatedRoute
} from '@angular/router';
import {
  FormControl,
  FormGroup,
  FormBuilder,
  Validators
} from '@angular/forms';
import { LoginService } from './../../../services/auth/login.service';
import { User } from './../../../models/user.model';
import { Config } from './../../../environments/config';

@Component({
  selector: 'login',
  styleUrls: ['./login.component.css'],
  templateUrl: './login.component.html',
  providers: [LoginService],
})

export class LoginComponent implements OnInit {

  public formErrors;

  /**
   * Form Controls
   */
  public loginForm: FormGroup;

  constructor(
    public route: ActivatedRoute,
    private loginService: LoginService,
    private fb: FormBuilder,
    private router: Router
  ) {

  }

  public ngOnInit() {
    this.createForm();
  }

  public createForm() {
    this.loginForm = this.fb.group({
      email: ['', Validators.compose([Validators.required, Validators.minLength(3)])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(8)])]
    });
    this.loginForm.valueChanges.subscribe((data) => (this.onValueChanged(data)));
    this.onValueChanged();    

  }

  public onValueChanged(data?: any) {

    if (!this.loginForm) {
      return;
    }
    for (let key in this.formErrors) {

      if (!this.formErrors[key]) {
        continue;
      }
      this.formErrors[key] = '';
    }

  }

  public onSubmit() {
    let params: any = {
      grant_type: 'password',
      client_id: Config.get('clientID'),
      client_secret: Config.get('clientSecret'),
      username: this.loginForm.value.email,
      password: this.loginForm.value.password
    };
    this.loginService.login(params).subscribe(
      (data) => {
        localStorage.setItem('currentUser', (String)(data.id));
        this.router.navigate(['users/profile']);
      },
      (errors) => {
        this.formErrors = errors;
      }
    );

  }

}
