/**
 * @author Mustafa Omran <m.omran@mQuBits.com>
 */
import {
  Component,
  OnInit
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { genders } from './../../../models/user.model';
import { RegisterService } from './../../../services/auth/register.service';
import { User } from './../../../models/user.model';
import { Config } from './../../../environments/config';
import { Router } from '@angular/router';
import { GeoIPService } from './../../../services/geoip/geoip.service';

/**
 *  jQuery variables
 */
declare var jquery: any;
declare var $: any;

@Component({
  selector: 'register',
  styleUrls: ['./register.component.css'],
  templateUrl: './register.component.html',
  providers: [
    RegisterService,
    GeoIPService
  ],
})
export class RegisterComponent implements OnInit {
  public genders: String[] = genders;
  public maxDate: Date;
  public formErrors = {
    email: '',
    password: '',
    name: '',
    mobile: '',
    age: '',
    gender: ''
  };
  public errors: any;

  /**
   * Form Controls
   */
  public registrationForm: FormGroup;

  constructor(
    public route: ActivatedRoute,
    private registerService: RegisterService,
    private geoipService: GeoIPService,
    private fb: FormBuilder,
    private router: Router,
  ) {

  }

  public ngOnInit() {
    this.geoipService.locate().subscribe(
      (data) => {
        if (this.registrationForm) {
          this.registrationForm.patchValue({
            ip: data.ip,
            country: data.country,
            city: data.city,
            state: data.state_name,
            postalCode: data.postal_code,
            latitude: data.lat,
            longitude: data.lon,
            timezone: data.timezone,
            continent: data.continent,
            currency: data.currency,
          });
        }
      },
      (errors) => {
        this.errors = errors;
      }
    );
    this.maxDate = new Date();
    this.maxDate.setFullYear(this.maxDate.getFullYear() - 13);
    this.createForm();
  }

  public createForm() {
    this.registrationForm = this.fb.group({
      email: ['', Validators.compose([Validators.required, Validators.minLength(3)])],
      mobile: ['', Validators.compose([Validators.required, Validators.minLength(7), Validators.maxLength(14), Validators.required])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
      firstName: ['', Validators.compose([Validators.minLength(6)])],
      lastName: ['', Validators.compose([Validators.minLength(6)])],
      birthdate: ['', Validators.compose([Validators.required])],
      gender: ['Male', Validators.compose([Validators.required])],
      address: ['', Validators.compose([])],
      apartment: ['', Validators.compose([])],
      floor: ['', Validators.compose([])],
      building: ['', Validators.compose([])],
      street: ['', Validators.compose([])],
      district: ['', Validators.compose([])],
      city: ['', Validators.compose([])],
      state: ['', Validators.compose([])],
      country: ['', Validators.compose([])],
      postalCode: ['', Validators.compose([])],
      longitude: ['', Validators.compose([])],
      latitude: ['', Validators.compose([])],
      continent: ['', Validators.compose([])],
      ip: ['', Validators.compose([])],
      timezone: ['', Validators.compose([])],
      currency: ['', Validators.compose([])]
    });

    this.registrationForm.valueChanges.subscribe((data) => (this.onValueChanged(data)));

    this.onValueChanged();
  }

  public onValueChanged(data?: any) {
    if (!this.registrationForm) {
      return;
    }
    for (let key in this.formErrors) {
      if (!this.formErrors[key]) {
        continue;
      }
      this.formErrors[key] = '';
    }
  }

  public prepareUser(formValue: any): User {
    let ret: User = new User(
      0,
      formValue.email,
      formValue.createdAt,
      formValue.updatedAt,
      formValue.mobile,
      formValue.firstName,
      formValue.lastName,
      formValue.address,
      formValue.district,
      formValue.city,
      formValue.state,
      formValue.country,
      formValue.birthdate,
      formValue.profileImage,
      formValue.userID,
      formValue.gender,
      formValue.paymobPaymentToken,
      formValue.deletedAt,
      formValue.postalCode,
      formValue.longitude,
      formValue.latitude,
      formValue.timezone,
      formValue.continent,
      formValue.currency,
      formValue.ip,
      formValue.image,
      formValue.apartment,
      formValue.floor,
      formValue.building,
      formValue.street
    );
    return ret;
  }

  public onSubmit() {
    let params: User = this.prepareUser(this.registrationForm.value);
    this.registerService.register(params).subscribe(
      (data) => {
        localStorage.setItem('currentUser', (String)(data.id));
        this.router.navigate(['users/profile']);
      },
      (errors) => {
        this.formErrors = errors;
      }
    );
  }


}
