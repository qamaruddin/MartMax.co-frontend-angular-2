import { ActivatedRoute, Data } from '@angular/router';
import { Component } from '@angular/core';
import { inject, TestBed } from '@angular/core/testing';

// Load the implementations that should be tested
import { TopBrandsComponent } from './topbrands.component';

describe('TopBrands', () => {
  // provide our implementations or mocks to the dependency injector
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      // provide a better mock
      {
        provide: ActivatedRoute,
        useValue: {
          data: {
            subscribe: (fn: (value: Data) => void) => fn({
              yourData: 'yolo'
            })
          }
        }
      },
      BrandsComponent
    ]
  }));

  it('should log ngOnInit', inject([TopBrandsComponent], (topbrands: TopBrandsComponent) => {
    spyOn(console, 'log');
    expect(console.log).not.toHaveBeenCalled();
    topbrands.ngOnInit();
    expect(console.log).toHaveBeenCalled();
  }));

});
