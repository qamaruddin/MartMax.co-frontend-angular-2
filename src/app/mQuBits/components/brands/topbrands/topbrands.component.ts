/**
 * @author Mustafa Omran <m.omran@mQuBits.com>
 */

import {
  Component,
  OnInit
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Config } from './../../../environments/config';
import { BrandsService } from './../../../services/brands/brands.service';

@Component({
  selector: 'martmax-top-brands',
  styleUrls: ['./topbrands.component.css'],
  templateUrl: './topbrands.component.html',
  providers: [BrandsService],
})
export class TopBrandsComponent implements OnInit {
  config: Object = {
    paginationClickable: true,
    spaceBetween: 0,
    slidesPerView: 6,
    slidesPerColumn: 2,
    autoplay: 2000,
    grabCursor: true,
    pagination: '.swiper-pagination',
  };

  public errors: any;
  public topbrands: any;
  public keys: any;

  constructor(
    public route: ActivatedRoute,
    public brandsService: BrandsService,
  ) {

  }

  public ngOnInit() {
    /**
     * top brands
     */
    this.brandsService.list()
      .subscribe(
      (data) => {
        if (data.length === 0) {
          return;
        }
        this.topbrands = data.top;
        console.log(data.top);
      },
      (errors) => {
        this.errors = errors;
      }
      );
  }

  public setBrand(brand) {
    localStorage.setItem('currentBrand', JSON.stringify(brand));
  }
}
