/**
 * @author Mustafa Qamar-ud-Din <m.qamaruddin@mQuBits.com>
 */
import {
  Component,
  OnInit
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'martmax-contact-us',
  styleUrls: ['./contact-us.component.css'],
  templateUrl: './contact-us.component.html',
  providers: []
})
export class ContactUsComponent implements OnInit {
  public errors: any;

  constructor(
    public route: ActivatedRoute
  ) {
  }

  public ngOnInit() {
      //
  }
}
