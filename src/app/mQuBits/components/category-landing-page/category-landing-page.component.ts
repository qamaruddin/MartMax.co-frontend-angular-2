/**
 * @author Mustafa Qamar-ud-Din <m.qamaruddin@mQuBits.com>
 */
import {
  Component,
  OnInit,
  Input,
  Attribute,
  ElementRef,
  EventEmitter,
  Output
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'category-landing-page',
  styleUrls: ['./category-landing-page.component.css'],
  templateUrl: './category-landing-page.component.html',
  providers: [
  ]
})

export class CategorylandingpageComponent implements OnInit {

  public keyword: any;
  public category: any;
  public pagination: any;

  constructor(
    public route: ActivatedRoute
  ) {
      //
  }

  public ngOnInit() {
      this.route.params.subscribe(params=> {
        this.keyword = params['keyword'];
        this.category = params['category'];
        this.pagination = params['pagination'];
      });
  }

}
