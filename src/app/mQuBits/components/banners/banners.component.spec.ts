import { ActivatedRoute, Data } from '@angular/router';
import { Component } from '@angular/core';
import { inject, TestBed } from '@angular/core/testing';

// Load the implementations that should be tested
import { BannersComponent } from './banners.component';

describe('Banners', () => {
  // provide our implementations or mocks to the dependency injector
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      // provide a better mock
      {
        provide: ActivatedRoute,
        useValue: {
          data: {
            subscribe: (fn: (value: Data) => void) => fn({
              yourData: 'yolo'
            })
          }
        }
      },
      BannersComponent
    ]
  }));

  it('should log ngOnInit', inject([BannersComponent], (banners: BannersComponent) => {
    spyOn(console, 'log');
    expect(console.log).not.toHaveBeenCalled();
    banners.ngOnInit();
    expect(console.log).toHaveBeenCalled();
  }));

});
