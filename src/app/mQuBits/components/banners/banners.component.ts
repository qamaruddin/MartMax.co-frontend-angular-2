/**
 * @author Mustafa Omran <m.omran@mQuBits.com>
 */

import {
  Component,
  OnInit
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Config } from './../../../environments/config';
import { BannersService } from './../../services/banners/banners.service';

@Component({
  selector: 'martmax-banners',
  styleUrls: ['./banners.component.css'],
  templateUrl: './banners.component.html',
  providers: [BannersService],
})
export class BannersComponent implements OnInit {
  /**
   * slider
   */
  config: Object = {
    pagination: '.swiper-pagination',
    paginationClickable: true,
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
    slidesPerView: 1,
    autoplay: 2500,
    loop: true,
    spaceBetween: 30
  };

  public errors: any;
  public banners: any;
  public keys: any;

  constructor(
    public route: ActivatedRoute,
    public bannersService: BannersService,
  ) {
  }

  public ngOnInit() {
    this.bannersService.list()
      .subscribe(
      (data) => {
        if (data.length === 0) {
          return;
        }
        this.banners = data;
      },
      (errors) => {
        this.errors = errors;
      }
      );
  }
}
