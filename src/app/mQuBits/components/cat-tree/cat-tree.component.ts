/**
 * @author Mustafa Qamar-ud-Din <m.qamaruddin@mQuBits.com>
 */
import {
	Component,
	OnInit,
	Input,
	Attribute,
	ElementRef,
	EventEmitter,
	Output
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CattreeService } from './../../services/products/cat-tree.service';
/**
 *  jQuery variables
 */
declare var jquery: any;
declare var $: any;

@Component({
	selector: 'cat-tree',
	styleUrls: ['./cat-tree.component.css'],
	templateUrl: './cat-tree.component.html',
	providers: [
		CattreeService
	]
})

export class CattreeComponent implements OnInit {

	public categories: any;
	public errors: any;
	public config: any;

	constructor(
		public route: ActivatedRoute,
		public cattreeService: CattreeService
	) {
		//
	}

	public ngOnInit() {
		this.config = {
			isShowAllCheckBox: true,
	      		isShowFilter: false,
		        isShowCollapseExpand: false,
		    	maxHeight: 500
		};
		this.cattreeService.list()
			.subscribe(
				(data) => {
	        			this.categories = data;
				      },
			      	(errors) => {
			              	this.errors = errors;
				      }
	          	);
	}

	public onSelectedChange(event) {
		console.log(event);
	}
}

