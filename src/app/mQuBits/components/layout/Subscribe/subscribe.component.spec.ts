import { ActivatedRoute, Data } from '@angular/router';
import { Component } from '@angular/core';
import { inject, TestBed } from '@angular/core/testing';

// Load the implementations that should be tested
import { SubscribeComponent } from './subscribe.component';

describe('Subscribe', () => {
  // provide our implementations or mocks to the dependency injector
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      // provide a better mock
      {
        provide: ActivatedRoute,
        useValue: {
          data: {
            subscribe: (fn: (value: Data) => void) => fn({
              yourData: 'yolo'
            })
          }
        }
      },
      SubscribeComponent
    ]
  }));

  it('should log ngOnInit', inject([SubscribeComponent], (subscribe: SubscribeComponent) => {
    spyOn(console, 'log');
    expect(console.log).not.toHaveBeenCalled();
    subscribe.ngOnInit();
    expect(console.log).toHaveBeenCalled();
  }));

});
