/**
 * @author Mustafa Omran <m.omran@mQuBits.com>
 */
import {
  Component,
  OnInit
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Config } from './../../../environments/config';

/**
 *  jQuery variables
 */
declare var jquery: any;
declare var $: any;

@Component({
  selector: 'martmax-footer',
  styleUrls: ['./footer.component.css'],
  templateUrl: './footer.component.html',
  providers: [],
})
export class FooterComponent implements OnInit {
  public errors: any;
  public angularclassLogo = 'http://res.cloudinary.com/dzl9mwemk/image/upload/v1492510643/1492171333_wf6hzr.png';
  public name = 'mQuBits';
  public url = 'https://facebook.com/mQuBits';

  constructor(
    public route: ActivatedRoute,
  ) {

  }

  public ngOnInit() {
    
  }

    /**
* jQuery
*/
public scrollToTop() {
  $(document).ready(function() {
    function checkPosition() {
        if ($(this).scrollTop() > 200) {
            $('.go-top').fadeIn(500);
        } else {
            $('.go-top').fadeOut(300);
        }
    }
    // Show or hide the sticky footer button
    $(window).scroll(checkPosition);

    // Animate the scroll to top
    $('.go-top').click(function(event) {
        event.preventDefault();

        $('html, body').animate({scrollTop: 0}, 300);
    })

    checkPosition();
});
}
}
