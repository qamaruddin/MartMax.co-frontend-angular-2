/**
 * @author Mustafa Qamar-ud-Din <m.qamaruddin@mQuBits.com>
 */
import {
  Component,
  OnInit
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Config } from './../../../environments/config';

@Component({
  selector: 'why-us',
  styleUrls: ['./whyus.component.css'],
  templateUrl: './whyus.component.html',
  providers: [],
})
export class WhyUsComponent implements OnInit {
  public errors: any;

  constructor(
    public route: ActivatedRoute,
  ) {

  }

  public ngOnInit() {

  }

}
