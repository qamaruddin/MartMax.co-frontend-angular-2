import { ActivatedRoute, Data } from '@angular/router';
import { Component } from '@angular/core';
import { inject, TestBed } from '@angular/core/testing';

// Load the implementations that should be tested
import { WhyUsComponent } from './whyus.component';

describe('WhyUs', () => {
  // provide our implementations or mocks to the dependency injector
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      // provide a better mock
      {
        provide: ActivatedRoute,
        useValue: {
          data: {
            subscribe: (fn: (value: Data) => void) => fn({
              yourData: 'yolo'
            })
          }
        }
      },
      WhyUs
    ]
  }));

  it('should log ngOnInit', inject([WhyUs], (whyus: WhyUs) => {
    spyOn(console, 'log');
    expect(console.log).not.toHaveBeenCalled();
    whyus.ngOnInit();
    expect(console.log).toHaveBeenCalled();
  }));

});
