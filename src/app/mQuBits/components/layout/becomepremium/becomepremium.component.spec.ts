import { ActivatedRoute, Data } from '@angular/router';
import { Component } from '@angular/core';
import { inject, TestBed } from '@angular/core/testing';

// Load the implementations that should be tested
import { BecomePremiumComponent } from './becomepremium.component';

describe('BecomePremium', () => {
  // provide our implementations or mocks to the dependency injector
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      // provide a better mock
      {
        provide: ActivatedRoute,
        useValue: {
          data: {
            subscribe: (fn: (value: Data) => void) => fn({
              yourData: 'yolo'
            })
          }
        }
      },
      BecomePremiumComponent
    ]
  }));

  it('should log ngOnInit', inject([BecomePremiumComponent], (becomepremium: BecomePremiumComponent) => {
    spyOn(console, 'log');
    expect(console.log).not.toHaveBeenCalled();
    becomepremium.ngOnInit();
    expect(console.log).toHaveBeenCalled();
  }));

});
