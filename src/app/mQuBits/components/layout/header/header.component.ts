/**
 * @author Mustafa Omran <m.omran@mQuBits.com>
 */
import {
  Component,
  OnInit
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Config } from './../../../environments/config';
import { CouponsService } from './../../../services/coupons/coupons.service';
import { Coupon } from './../../../models/coupon.model';
import { CartService } from './../../../services/cart/cart.service';

@Component({
  selector: 'martmax-header',
  styleUrls: ['./header.component.css'],
  templateUrl: './header.component.html',
  providers: [
    CouponsService,
    CartService],
})
export class HeaderComponent {
  public errors: any;
  public coupon: Coupon;
    public countdown: any;
    public carttotalitems: number;

  constructor(
    public route: ActivatedRoute,
    public couponsService: CouponsService,
    public cartService: CartService,
    public router: Router,
  ) {

  }

  public ngOnInit() {
    /* coupons service */
    this.couponsService.list()
      .subscribe(
      (data) => {
        if (!data || data.length === 0 || data === '' || data === null || data === undefined || data === 'null' || data === 'undefined') {
          return;
        }
        let myCoupon = new Coupon(data.id, data.code, data.type, data.value, data.redeem_datetime, data.deleted_at, data.created_at, data.updated_at);
        this.coupon = myCoupon;
        this.countdown = this.coupon.timer();
      },
      (errors) => {
        this.errors = errors;
      }
      );
      this.carttotalitems = this.cartService.cartTotalItem();
  }
}
