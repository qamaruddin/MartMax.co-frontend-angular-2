/**
 * @author Mustafa Qamar-ud-Din <m.qamaruddin@mQuBits.com>
 * @author Abdelazem Eid <a.eid@mQuBits.com>
 */
import {
  Component,
  OnInit
} from '@angular/core';
import { Config } from './../../../environments/config';

@Component({
  selector: 'martmax-sticky',
  styleUrls: ['./stickybar.component.css'],
  templateUrl: './stickybar.component.html',
  providers: []
})

export class StickybarComponent implements OnInit {

  constructor(
  ){
  } 
  
  public ngOnInit() {
    //   
  }

}
