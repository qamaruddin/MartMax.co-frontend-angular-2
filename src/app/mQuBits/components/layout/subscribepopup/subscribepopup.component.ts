/**
 * @author Mustafa Omran <m.omran@mQuBits.com>
 */

import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'martmax-subscribe-popoup',
  styleUrls: ['./subscribepopup.component.css'],
  templateUrl: './subscribepopup.component.html',
  providers: [],
})
export class SubscribePopupComponent {

  /**
   *  validation
   */
  public loginForm = this.fb.group({
    email: ['', Validators.required],
  });

  constructor(public fb: FormBuilder) { }

  public doLogin(event) {
    // email
    let email = this.loginForm.controls.email.value;
  }

  /**
   * close subscribe
   */
  public close(event) {
    event.preventDefault();

    let y = document.getElementById('subscribe');
    if (typeof y !== 'undefined') {
      y.style.display = 'none';
    }
  }
}
