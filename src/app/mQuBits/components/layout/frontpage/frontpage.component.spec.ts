import { ActivatedRoute, Data } from '@angular/router';
import { Component } from '@angular/core';
import { inject, TestBed } from '@angular/core/testing';

// Load the implementations that should be tested
import { FrontpageComponent } from './frontpage.component';

describe('Frontpage', () => {
  // provide our implementations or mocks to the dependency injector
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      // provide a better mock
      {
        provide: ActivatedRoute,
        useValue: {
          data: {
            subscribe: (fn: (value: Data) => void) => fn({
              yourData: 'yolo'
            })
          }
        }
      },
      FrontpageComponent
    ]
  }));

  it('should log ngOnInit', inject([FrontpageComponent], (frontpage: FrontpageComponent) => {
    spyOn(console, 'log');
    expect(console.log).not.toHaveBeenCalled();

    frontpage.ngOnInit();
    expect(console.log).toHaveBeenCalled();
  }));

});
