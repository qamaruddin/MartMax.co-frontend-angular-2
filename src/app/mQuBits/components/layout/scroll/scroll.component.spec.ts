import { ActivatedRoute, Data } from '@angular/router';
import { Component } from '@angular/core';
import { inject, TestBed } from '@angular/core/testing';

// Load the implementations that should be tested
import { ScrollComponent } from './scroll.component';

describe('Scroll', () => {
  // provide our implementations or mocks to the dependency injector
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      // provide a better mock
      {
        provide: ActivatedRoute,
        useValue: {
          data: {
            subscribe: (fn: (value: Data) => void) => fn({
              yourData: 'yolo'
            })
          }
        }
      },
      ScrollComponent
    ]
  }));

  it('should log ngOnInit', inject([ScrollComponent], (scroll: ScrollComponent) => {
    spyOn(console, 'log');
    expect(console.log).not.toHaveBeenCalled();
    scroll.ngOnInit();
    expect(console.log).toHaveBeenCalled();
  }));

});
