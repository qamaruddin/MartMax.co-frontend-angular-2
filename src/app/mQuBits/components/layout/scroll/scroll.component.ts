/**
 * @author Mustafa Omran <m.omran@mQuBits.com>
 */
import {
  Component,
  OnInit
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Config } from './../../../environments/config';

/**
 *  jQuery variables
 */
declare var jquery: any;
declare var $: any;

@Component({
  selector: 'martmax-scroll',
  styleUrls: ['./scroll.component.css'],
  templateUrl: './scroll.component.html',
  providers: [],
})
export class ScrollComponent implements OnInit {

  public errors: any;

  constructor(
    public route: ActivatedRoute,
  ) {

  }

  public ngOnInit() {
    $(window).scroll(function () {
      if ($(this).scrollTop() > 200) {
        $("#backtotop").addClass("active");
      } else {
        $("#backtotop").removeClass("active");
      }
    });

    $('#backtotop').on("click", function () {
      $('body,html').animate({
        scrollTop: 0
      }, 1000);
      return false;
    });
  }

}
