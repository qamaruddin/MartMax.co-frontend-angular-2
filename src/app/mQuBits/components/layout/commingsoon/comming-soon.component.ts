/**
 * @author Mustafa Omran <m.Omran@mQuBits.com>
 */
import {
  Component,
  OnInit
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Config } from './../../../environments/config';

@Component({
  selector: 'martmax-comming-soon',
  styleUrls: ['./comming-soon.component.css'],
  templateUrl: './comming-soon.component.html',
  providers: [],
})
export class CommingSoonComponent implements OnInit {
  public errors: any;

  constructor(
    public route: ActivatedRoute,
  ) {

  }

  public ngOnInit() {

  }

}
