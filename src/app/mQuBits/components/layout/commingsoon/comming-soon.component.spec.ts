import { ActivatedRoute, Data } from '@angular/router';
import { Component } from '@angular/core';
import { inject, TestBed } from '@angular/core/testing';

// Load the implementations that should be tested
import { CommingSoonComponent } from './comming-soon.component';

describe('CommingSoon', () => {
  // provide our implementations or mocks to the dependency injector
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      // provide a better mock
      {
        provide: ActivatedRoute,
        useValue: {
          data: {
            subscribe: (fn: (value: Data) => void) => fn({
              yourData: 'yolo'
            })
          }
        }
      },
      CommingSoonComponent
    ]
  }));

  it('should log ngOnInit', inject([CommingSoonComponent], (CommingSoon: CommingSoonComponent) => {
    spyOn(console, 'log');
    expect(console.log).not.toHaveBeenCalled();
    CommingSoon.ngOnInit();
    expect(console.log).toHaveBeenCalled();
  }));

});
