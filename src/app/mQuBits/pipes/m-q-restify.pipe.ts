import {
    Pipe,
    PipeTransform
} from '@angular/core';

/*
 * Strips double quotes & sanitize image
 * Takes a json object attribute
 * Usage:
 *   value | mqrestify
 * Example:
 *   {{ "unsafe:http://" |  mqrestify}}
 *   formats to: http://
*/

@Pipe({name: 'mQRestify'})
export class MQRestifyPipe implements PipeTransform {
  public transform(value: any): any {
    if (!value) {
      return value;
    }
    return value.replace (/(^")|("$)/g, '');
  }
}
