<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CopyAliExpressCats extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        DB::execute("ALTER TABLE categories DROP INDEX categories_category_unique;");
        Schema::table('categories', function(Blueprint $table) {
            $table->dropUnique('categories_category_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //
    }

}
